import {block} from '../utils';
import {TextBlock,TitleBlock,ColumnsBlock,ImageBlock} from './blocks';
export class Sidebar {
	constructor(selector, updateCallback) {
		this.el = document.querySelector(selector);
		this.update = updateCallback;
		this.init();
	}
	init(){
		this.el.insertAdjacentHTML('afterbegin', this.template);
		this.el.addEventListener('submit', this.addBlock.bind(this));
	}
	get template (){
		return [
			block('title'),
			block('text'),
			block('image'),
		].join('');
	}
	addBlock(event) {
		event.preventDefault();
		
		const type = event.target.name;
		const value = event.target.value.value;
		const styles = event.target.styles.value;

		let newBlock = type === 'text'
			? newBlock = new TextBlock(value,{styles})
			: newBlock = new TitleBlock(value,{styles});

		this.update(newBlock);
		event.target.value.value = '';
		event.target.styles.value = '';
		
		/*console.log(type);
		console.log(value);
		console.log(styles);*/

/*		let newBlock;
		if(type == 'text'){
			newBlock = new TextBlock(value,{styles:styles});
		}else{
			newBlock = new TitleBlock(value,{styles:styles});
		}*/

		
	}
}


