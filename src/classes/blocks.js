import{row, col, css} from "../utils";
class Block {
	constructor(value, options) {
		this.value = value;
		this.options = options;
	}
	toHTML(){
		throw new Error('метод toHTML должен быть реализован');
	}
}

export class TitleBlock extends Block{
	constructor(value, options){
		super(value, options);
	}
	toHTML(){
		const {tag = "h1", styles} = this.options; // Если не указано будет h1
		return row(col(`<${tag}>${this.value}</${tag}>`), css(styles));
	}
}
export class TextBlock extends Block{
	constructor(value, options){
		super(value, options);
	}
	toHTML(){
		return row(col(`<p>${this.value}</p>`), css(this.options.styles));
	}
}
export class ColumnsBlock extends Block{
	constructor(value, options){
		super(value, options);
	}
	toHTML(){
	const html = this.value.map(col).join(''); //,было так (item => col(item))
	return row(html,css(this.options.styles)); //join('') убираем запятые лишние
	}
}
export class ImageBlock extends Block{
	constructor(value, options){
		super(value, options);
	}
	toHTML(){
		return row(`<img src="${this.value}"/>`, css(this.options.styles));
	}
}