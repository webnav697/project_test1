import image from "./assets/banner-2.jpg";
import {TextBlock,TitleBlock,	ColumnsBlock,	ImageBlock} from "./classes/blocks";

export const model = [ // Создаём массив, в нём объекты
	new TitleBlock('Заголовок h1', {
		tag: 'h2',
		styles: {
			color: '#fff',
			background: '#ff0099',
			padding: '1.5 rem',
			'text-align': 'center',
		}
	}),
	new ImageBlock(image, {
		styles: {
			width: '300px',
			height: 'auto',
			margin: '20px auto'
		}
	}),
	new TextBlock("1 Lorem ipsum dolor sit amet", {
		styles: {
			color:'#222',
		}
	}),
	new ColumnsBlock([
		"1 Lorem ipsum dolor sit amet",
		"2 Lorem ipsum dolor sit amet",
		"3 Lorem ipsum dolor sit amet"
	],{
		styles: {
			color: '#999',
			margin:'20px 0 40px'
		}
	}),
];