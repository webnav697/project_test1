import {model} from "./model";
import {App} from "./classes/app";

import "./css/template_style.css";
import "./css/style.css";

new App(model).init();